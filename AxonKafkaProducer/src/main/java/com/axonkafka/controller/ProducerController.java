package com.axonkafka.controller;

import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.axonkafka.command.UserCreatedCommand;

@RestController
@RequestMapping("/api")
public class ProducerController {

	@Autowired
	private CommandGateway commandGateway;

	
	@GetMapping("/send")
	public void send(){
		String userId = UUID.randomUUID().toString();
		UserCreatedCommand command = new UserCreatedCommand(userId, "test");
		commandGateway.send(command);
	}
}
