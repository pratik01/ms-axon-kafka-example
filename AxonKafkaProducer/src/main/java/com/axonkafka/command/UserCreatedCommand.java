package com.axonkafka.command;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

public class UserCreatedCommand {

	@TargetAggregateIdentifier
	private String userId;
	private String name;

	public UserCreatedCommand() {

	}

	public UserCreatedCommand(String userId, String name) {
		super();
		this.userId = userId;
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "UserCreatedCommand [userId=" + userId + ", name=" + name + "]";
	}

}
