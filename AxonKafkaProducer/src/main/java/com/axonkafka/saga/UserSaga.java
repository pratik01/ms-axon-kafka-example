package com.axonkafka.saga;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.saga.EndSaga;
import org.axonframework.eventhandling.saga.SagaEventHandler;
import org.axonframework.eventhandling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import com.axonkafka.command.CreateWalletCommand;
import com.axonkafka.events.UserCreatedEvent;
import com.axonkafka.events.WalletCreatedEvent;

@Saga(configurationBean="mySagaConfiguration")
@ProcessingGroup("walletSagaProcessor")	
public class UserSaga {

	@Autowired
	private transient CommandGateway commandGateway;

	private String userId;
	private String name;
	private String walletId;

	public UserSaga() {

	}

	@StartSaga
	@SagaEventHandler(associationProperty = "userId")
	public void on(UserCreatedEvent event) {
		System.out.println("Saga start");	
		this.userId = event.getUserId();
		this.name = event.getName();
		commandGateway.send(new CreateWalletCommand(event.getUserId(), event.getName()));
	}

	@EndSaga
	@SagaEventHandler(associationProperty = "userId")
	public void on(WalletCreatedEvent event) {
		System.out.println("Saga end");
		this.userId = event.getUserId();
		this.walletId = event.getWalletId();
		//SagaLifecycle.end();
	}	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
}
