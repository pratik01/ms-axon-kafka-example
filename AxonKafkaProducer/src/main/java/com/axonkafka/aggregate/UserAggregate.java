package com.axonkafka.aggregate;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import com.axonkafka.command.CreateWalletCommand;
import com.axonkafka.command.UserCreatedCommand;
import com.axonkafka.events.CreateWalletEvent;
import com.axonkafka.events.UserCreatedEvent;

@Aggregate
public class UserAggregate {

	@AggregateIdentifier
	private String userId;

	private String name;
	
	@CommandHandler
	public UserAggregate(UserCreatedCommand command){
		System.out.println("Aggregate : command handler");
		AggregateLifecycle.apply(new UserCreatedEvent(command.getUserId(),command.getName()));
	}

	@EventSourcingHandler
	public void handle(UserCreatedEvent event){
		System.out.println("Aggregate : EventSourcingHandler");
		this.userId = event.getUserId();
		this.name = event.getName();
	}
	
	@CommandHandler
	public void handle(CreateWalletCommand createWalletCommand) {
		System.out.println("\n\nAggregate : Inside CreateWalletCommand Handler\n\n");
		AggregateLifecycle.apply(new CreateWalletEvent(createWalletCommand.getUserId(),createWalletCommand.getName()));
		System.out.println("\n\nnAggregate: Exit CreateWalletCommand Handler\n\n");
	}
	
	@EventSourcingHandler
	public void on(CreateWalletEvent createWalletEvent){
		System.out.println("Inside CreateWalletEvent sourcing handler");
		this.userId = createWalletEvent.getUserId();
		this.name = createWalletEvent.getName();
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "UserAggregate [userId=" + userId + ", name=" + name + "]";
	}

}
