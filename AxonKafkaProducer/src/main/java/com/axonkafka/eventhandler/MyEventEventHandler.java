package com.axonkafka.eventhandler;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import com.axonkafka.events.WalletCreatedEvent;

/*
 If ProcessingGroup is not set, then the package name is used.
 The name of the ProcessingGroup is also in the configuration
 and the name must match the name of the TrackingProcessor defined
 in the configuration. Axon adds automatically all the handler to the TrackingProcessor
 The class must be a @Component
 */
@Component
//@ProcessingGroup("WalletProcessor")
public class MyEventEventHandler {	

	/*@EventHandler
	public void handleMyEvent(WalletCreatedEvent event) {
		System.out.println("\n\ngot the WalletCreatedEvent {}" + event);
	}*/

}
	