package com.axonkafka.events;

public class CreateWalletEvent {

	private String userId;
	private String name;

	public CreateWalletEvent() {

	}

	public CreateWalletEvent(String userId, String name) {
		super();
		this.userId = userId;
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CreateWalletEvent [userId=" + userId + ", name=" + name + "]";
	}

}
