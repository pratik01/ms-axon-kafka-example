package com.axonkafka.events;

public class CreateUserProfileFailedEvent {

	private Long userId;

	public CreateUserProfileFailedEvent(Long userId) {
		super();
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
