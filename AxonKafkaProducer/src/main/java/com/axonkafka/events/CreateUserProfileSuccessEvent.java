package com.axonkafka.events;

public class CreateUserProfileSuccessEvent {
	private Long userId;

	public CreateUserProfileSuccessEvent(Long userId) {
		super();
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
