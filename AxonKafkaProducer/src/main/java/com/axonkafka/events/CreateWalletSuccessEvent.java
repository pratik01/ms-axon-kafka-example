package com.axonkafka.events;

public class CreateWalletSuccessEvent {

	private Long userId;
	private String walletId;

	public CreateWalletSuccessEvent(Long id, String walletId) {
		super();		
		this.walletId = walletId;
		this.userId = id;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setId(Long id) {
		this.userId = id;
	}

}
