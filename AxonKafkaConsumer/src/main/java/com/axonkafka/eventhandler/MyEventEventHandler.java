package com.axonkafka.eventhandler;

import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axonkafka.command.WalletCreatedCommand;
import com.axonkafka.events.CreateWalletEvent;

/*
 If ProcessingGroup is not set, then the package name is used.
 The name of the ProcessingGroup is also in the configuration
 and the name must match the name of the TrackingProcessor defined
 in the configuration. Axon adds automatically all the handler to the TrackingProcessor
 The class must be a @Component
 */
@Component
@ProcessingGroup("MyProcessor")
public class MyEventEventHandler {		
	
	@Autowired
	private transient CommandGateway commandGateway;
	
	@EventHandler
	public void handleMyEvent(CreateWalletEvent event) {
		System.out.println("\n\ngot the event {}" + event);
		String walletId = UUID.randomUUID().toString();
		WalletCreatedCommand command = new WalletCreatedCommand(walletId,event.getUserId());
		System.out.println("WalletCreatedCommand ==> "+command);
		commandGateway.send(command);		
	}

}
