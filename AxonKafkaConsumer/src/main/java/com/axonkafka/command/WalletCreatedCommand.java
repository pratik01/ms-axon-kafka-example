package com.axonkafka.command;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

public class WalletCreatedCommand {
	@TargetAggregateIdentifier
	private String walletId;
	private String userId;

	public WalletCreatedCommand(){
		
	}
	
	public WalletCreatedCommand(String walletId, String userId) {
		super();
		this.walletId = walletId;
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "WalletCreatedCommand [walletId=" + walletId + ", userId=" + userId + "]";
	}

}
