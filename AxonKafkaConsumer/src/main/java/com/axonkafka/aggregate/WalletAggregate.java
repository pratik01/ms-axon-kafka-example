package com.axonkafka.aggregate;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import com.axonkafka.command.WalletCreatedCommand;
import com.axonkafka.events.WalletCreatedEvent;
	
@Aggregate
public class WalletAggregate {

	@AggregateIdentifier
	private String walletId;
	private String userId;

	@CommandHandler
	public WalletAggregate(WalletCreatedCommand command) {
		System.out.println("\n\nWalletAggregate : CommandHandler\n\n");
		AggregateLifecycle.apply(new WalletCreatedEvent(command.getWalletId(), command.getUserId()));
	}

	@EventSourcingHandler
	public void handle(WalletCreatedEvent event) {
		System.out.println("\n\nWalletAggregate : EventSourcingHandler\n\n");
		this.walletId = event.getWalletId();
		this.userId = event.getUserId();
		//sender.send(event);
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "WalletAggregate [walletId=" + walletId + ", userId=" + userId + "]";
	}

}
