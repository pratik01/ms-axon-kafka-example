package com.axonkafka.controllers;

import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.axonkafka.command.WalletCreatedCommand;

@RestController
@RequestMapping("/api")
public class WalletController {
	@Autowired
	private transient CommandGateway commandGateway;

	@GetMapping("/wallet")
	public void wallet() {
		String walletId = UUID.randomUUID().toString();
		String userId = UUID.randomUUID().toString();
		WalletCreatedCommand command = new WalletCreatedCommand(walletId, userId);
		commandGateway.send(command);
	}
}
