package com.axonkafka.events;

public class WalletCreatedEvent {

	private String walletId;
	private String userId;

	public WalletCreatedEvent() {

	}

	public WalletCreatedEvent(String walletId, String userId) {
		super();
		this.walletId = walletId;
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "WalletCreatedEvent [walletId=" + walletId + ", userId=" + userId + "]";
	}

}
